<?php

namespace Drupal\crm;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for products_crm_entity.
 */
class ProductsCrmEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
