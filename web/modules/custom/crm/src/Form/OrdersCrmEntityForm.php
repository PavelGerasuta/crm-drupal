<?php

namespace Drupal\crm\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Orders crm entity edit forms.
 *
 * @ingroup crm
 */
class OrdersCrmEntityForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\crm\Entity\OrdersCrmEntity $entity */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
//    $products = $entity->get('field_products')->getString();
//    $length_products = strlen($products);
//    //Хранит многомерный массив информации о продутках
//    $array_product = [];
//
//    $k = 0;
//    $a = 0;
//
//    for ($i = 0; $i < $length_products; $i++) {
//      if ($products[$i] == ",") {
//        $a++;
//        if ($a > 1) {
//          $a = 0;
//          $k++;
//        }
//      }
//      if ($products[$i] != ",") {
//        if (empty($array_product[$k][$a])) {
//          $array_product[$k][$a] = "";
//        }
//        $array_product[$k][$a] .= $products[$i];
//      }
//    }
//
//
//    //Массив с индексами продуктах
//    $array_index = [];
//
//    for ($i = 0; $i <= $k; $i++) {
//      $array_index[$i] = $array_product[$i][0];
//    }
//
//    //Массив загруженных продуктав
//    $entity_product_array = \Drupal::entityTypeManager()->getStorage('products_crm_entity')->loadMultiple($array_index);
//
//    $flag = true;
//
//    for ($i = 0; $i <= $k; $i++){
//      $product_count = $array_product[$i][1];
//      foreach ($entity_product_array as $key => $entity_product){
//        if ($array_product[$i][0] == $key){
//          $entity_product_count = $entity_product->count->value;
//          if ($product_count <= $entity_product_count){
//            $count = $entity_product_count - $product_count;
//            $entity_product->count = $count;
//            $entity_product->save();
//          }else{
//            $flag = false;
//          }
//        }
//      }
//    }






   // if ($flag== true){
      $status = parent::save($form, $form_state);

      switch ($status) {
        case SAVED_NEW:
          $this->messenger()
            ->addMessage($this->t('Created the %label Orders crm entity.', [
              '%label' => $entity->label(),
            ]));
          break;

        default:
          $this->messenger()
            ->addMessage($this->t('Saved the %label Orders crm entity.', [
              '%label' => $entity->label(),
            ]));
      }
      $form_state->setRedirect('entity.orders_crm_entity.canonical', ['orders_crm_entity' => $entity->id()]);
//    }else{
//      \Drupal::messenger()->addError("Invalid number of products");
//    }


  }

}
