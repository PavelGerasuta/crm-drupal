<?php

namespace Drupal\crm\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Products crm entity entities.
 *
 * @ingroup products_for_crm
 */
class ProductsCrmEntityDeleteForm extends ContentEntityDeleteForm {


}
