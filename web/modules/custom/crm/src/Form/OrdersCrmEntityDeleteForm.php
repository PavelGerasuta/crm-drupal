<?php

namespace Drupal\crm\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Orders crm entity entities.
 *
 * @ingroup orders_for_crm
 */
class OrdersCrmEntityDeleteForm extends ContentEntityDeleteForm {


}
