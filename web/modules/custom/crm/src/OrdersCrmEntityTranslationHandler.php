<?php

namespace Drupal\crm;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for orders_crm_entity.
 */
class OrdersCrmEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
