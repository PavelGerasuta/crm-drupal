<?php

namespace Drupal\crm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Orders crm entity entities.
 *
 * @ingroup orders_for_crm
 */
class OrdersCrmEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Orders crm entity ID');
    $header['name'] = $this->t('Name order');
    $header['counterparties'] = $this->t('Counterparties');
    $header['user'] = $this->t('User order');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\crm\Entity\OrdersCrmEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.orders_crm_entity.edit_form',
      ['orders_crm_entity' => $entity->id()]
    );
    $row['counterparties'] = \Drupal\counterparties_crm\Entity\CounterpartiesEntity::load($entity->counterparties->getString())->label();
    $user =  \Drupal::entityTypeManager()->getStorage('user')->load($entity->user_id->getString());
    $row['user'] = $user->getAccountName();
    return $row + parent::buildRow($entity);
  }

}
