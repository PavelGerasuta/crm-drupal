<?php


namespace Drupal\crm;


class BuildingProduct {


  /**
   * @param $orders
   *
   * @return array
   *
   * Create array with users name and count orders
   */
  public function orders_users_build($orders){

    foreach ($orders as &$order){

      $order_user = \Drupal\user\Entity\User::load($order->get('user_id')->getString())->getAccountName();

      if (isset($orders_users[$order_user])){
        $orders_users[$order_user] ++;

      }else{
        $orders_users[$order_user] = 1;
      }

    }

    return $orders_users;
  }

  /**
   * @param $orders
   * Create array with users name and count orders
   * @return array
   */
  public function products_users_build($orders){

    //Цикл по заказам
    foreach ($orders as &$order){

      $product_user = \Drupal\user\Entity\User::load($order->get('user_id')->getString())->getAccountName();

      $products = $order->get('field_products')->getString();

      $building_product = new self();

      $check = 'sum_product';

      $products_sum = $building_product->transform_string_array($products,$check);



      if (isset($products_users[$product_user])){
        $products_users[$product_user] += $products_sum;

      }else{
        $products_users[$product_user] = $products_sum;
      }


    }

    return $products_users;
  }


  /**
   * @param $products
   * @param $check
   * create array with index and product count of string
   * @return int|mixed
   */
  public function transform_string_array($products,$check){

    $length_products = strlen($products);
    //Хранит многомерный массив информации о продутках
    $array_product = [];

    $k = 0;
    $a = 0;

    for ($i = 0; $i < $length_products; $i++) {
      if ($products[$i] == ",") {
        $a++;
        if ($a > 1) {
          $a = 0;
          $k++;
        }
      }
      if ($products[$i] != ",") {
        if (empty($array_product[$k][$a])) {
          $array_product[$k][$a] = "";
        }
        $array_product[$k][$a] .= $products[$i];
      }
    }

    //Массив с индексами продуктов
    $array_index = [];

    //Количесвто продуктов за заказ
    $sum_product = 0;

    for ($i = 0; $i <= $k; $i++) {
      $array_index[$i] = $array_product[$i][0];
      $array_product[$i][0] = str_replace(" ", '', $array_product[$i][0]);
      $array_product[$i][1] = str_replace(" ", '', $array_product[$i][1]);
      $sum_product += $array_product[$i][1];
    }

    if ($check == 'sum_product'){
      return $sum_product;
    }elseif($check == 'price'){
       return $array_product;
    }elseif ($check == 'index'){
      return $array_index;
    }else{
      return 0;
    }


  }


  /**
   * @param $orders
   * create array with index and price count of string
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function price_users_build($orders){

    foreach ($orders as &$order){

      $products = $order->get('field_products')->getString();



      $building_product = new self();

      $check = 'price';

      $sum_price = $building_product->transform_string_array($products,$check);

      for ($i = 0; $i < count($sum_price); $i++){

        $product_entity = \Drupal::entityTypeManager()->getStorage('products_crm_entity')->load($sum_price[$i][0]);

        $product_entity_price =  $product_entity->getPrice();

        $product_user = \Drupal\user\Entity\User::load($order->get('user_id')->getString())->getAccountName();

        $sum_price_product = $product_entity_price * $sum_price[$i][1];

        if (isset($products_users[$product_user])){
          $products_users[$product_user] += $sum_price_product;

        }else{
          $products_users[$product_user] = $sum_price_product;
        }

      }
    }

    return $products_users;

  }


}
