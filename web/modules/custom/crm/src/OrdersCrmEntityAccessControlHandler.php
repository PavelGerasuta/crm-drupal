<?php

namespace Drupal\crm;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Orders crm entity entity.
 *
 * @see \Drupal\crm\Entity\OrdersCrmEntity.
 */
class OrdersCrmEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\crm\Entity\OrdersCrmEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished orders crm entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published orders crm entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit orders crm entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete orders crm entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add orders crm entity entities');
  }


}
