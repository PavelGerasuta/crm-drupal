<?php

namespace Drupal\crm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Products crm entity entities.
 *
 * @ingroup products_for_crm
 */
class ProductsCrmEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Products crm entity ID');
    $header['name'] = $this->t('Name');
    $header['price'] = $this->t('Price');
    $header['count'] = $this->t('Count');
    $header['warehouse'] = $this->t('Warehouse');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\crm\Entity\ProductsCrmEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.products_crm_entity.edit_form',
      ['products_crm_entity' => $entity->id()]
    );
    $row['price'] = $entity->price->value;
    $row['count'] = $entity->count->value;
    $row['warehouse'] = $entity->warehouse->value;
    return $row + parent::buildRow($entity);
  }

}
