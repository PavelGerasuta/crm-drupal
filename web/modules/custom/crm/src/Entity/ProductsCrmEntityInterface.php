<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Products crm entity entities.
 *
 * @ingroup crm
 */
interface ProductsCrmEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Products crm entity name.
   *
   * @return string
   *   Name of the crm.
   */
  public function getName();

  /**
   * Sets the Products crm entity name.
   *
   * @param string $name
   *   The Products crm entity name.
   *
   * @return \Drupal\crm\Entity\ProductsCrmEntityInterface
   *   The called Products crm entity entity.
   */
  public function setName($name);

  /**
   * Gets the Products crm entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Products crm entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Products crm entity creation timestamp.
   *
   * @param int $timestamp
   *   The Products crm entity creation timestamp.
   *
   * @return \Drupal\crm\Entity\ProductsCrmEntityInterface
   *   The called Products crm entity entity.
   */
  public function setCreatedTime($timestamp);

}
