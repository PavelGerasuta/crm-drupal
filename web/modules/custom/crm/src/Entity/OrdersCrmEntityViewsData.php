<?php

namespace Drupal\crm\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Orders crm entity entities.
 */
class OrdersCrmEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
