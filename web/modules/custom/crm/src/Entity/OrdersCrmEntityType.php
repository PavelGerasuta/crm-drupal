<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Orders crm entity type entity.
 *
 * @ConfigEntityType(
 *   id = "orders_crm_entity_type",
 *   label = @Translation("Orders crm entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crm\OrdersCrmEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\crm\Form\OrdersCrmEntityTypeForm",
 *       "edit" = "Drupal\crm\Form\OrdersCrmEntityTypeForm",
 *       "delete" = "Drupal\crm\Form\OrdersCrmEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\crm\OrdersCrmEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "orders_crm_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "orders_crm_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export={
 *   "id",
 *   "label",
 *   "description",
 *   "weight"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/orders_crm_entity_type/{orders_crm_entity_type}",
 *     "add-form" = "/admin/structure/orders_crm_entity_type/add",
 *     "edit-form" = "/admin/structure/orders_crm_entity_type/{orders_crm_entity_type}/edit",
 *     "delete-form" = "/admin/structure/orders_crm_entity_type/{orders_crm_entity_type}/delete",
 *     "collection" = "/admin/structure/orders_crm_entity_type"
 *   }
 * )
 */
class OrdersCrmEntityType extends ConfigEntityBundleBase implements OrdersCrmEntityTypeInterface {

  /**
   * The Orders crm entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Orders crm entity type label.
   *
   * @var string
   */
  protected $label;

}
