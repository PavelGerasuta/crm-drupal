<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Products crm entity type entities.
 */
interface ProductsCrmEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
