<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Products crm entity entity.
 *
 * @ingroup products_for_crm
 *
 * @ContentEntityType(
 *   id = "products_crm_entity",
 *   label = @Translation("Products crm entity"),
 *   bundle_label = @Translation("Products crm entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crm\ProductsCrmEntityListBuilder",
 *     "views_data" = "Drupal\crm\Entity\ProductsCrmEntityViewsData",
 *     "translation" = "Drupal\crm\ProductsCrmEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\crm\Form\ProductsCrmEntityForm",
 *       "add" = "Drupal\crm\Form\ProductsCrmEntityForm",
 *       "edit" = "Drupal\crm\Form\ProductsCrmEntityForm",
 *       "delete" = "Drupal\crm\Form\ProductsCrmEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\crm\ProductsCrmEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\crm\ProductsCrmEntityAccessControlHandler",
 *   },
 *   base_table = "products_crm_entity",
 *   data_table = "products_crm_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer products crm entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name_product",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/products_crm_entity/{products_crm_entity}",
 *     "add-page" = "/admin/structure/products_crm_entity/add",
 *     "add-form" = "/admin/structure/products_crm_entity/add/{products_crm_entity_type}",
 *     "edit-form" = "/admin/structure/products_crm_entity/{products_crm_entity}/edit",
 *     "delete-form" = "/admin/structure/products_crm_entity/{products_crm_entity}/delete",
 *     "collection" = "/admin/structure/products_crm_entity",
 *   },
 *   bundle_entity_type = "products_crm_entity_type",
 *   field_ui_base_route = "entity.products_crm_entity_type.edit_form"
 * )
 */
class ProductsCrmEntity extends ContentEntityBase implements ProductsCrmEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * @return int
   */
  public function getPrice(){
    $price = $this->get('price')->value;
    if (isset($price)){
    return $price;
    }else {
      return 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * @return mixed
   */
  public function getOwnerName() {
    $owner_name = $this->get('user_id')->entity;
    return $owner_name->label();
  }


  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }


  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }



  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    //id
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Orders entity.'))
      ->setReadOnly(TRUE);

    //uuid
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Orders entity.'))
      ->setReadOnly(TRUE);


    //Имя продукта
    $fields['name_product'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name product'))
      ->setDescription(t('The name of the Products CRM entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Цена
    $fields['price'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Price product'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The price for one product'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      // Set no default value.
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    // Количество
    $fields['count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Count product'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('Number of products'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      // Set no default value.
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    //Изображения
    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setDescription(t('Image field'))
      ->setSettings([
        'file_directory' => 'IMAGE_FOLDER',
        'alt_field_required' => FALSE,
        'file_extensions' => 'png jpg jpeg',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'default',
        'weight' => -1,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'hidden',
        'type' => 'image_image',
        'weight' => -1,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    //Склад храниния
    $fields['warehouse'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Warehouse product'))
      ->setDescription(t('The warehouse where the product is stored in.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      // Set no default value.
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);



    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
