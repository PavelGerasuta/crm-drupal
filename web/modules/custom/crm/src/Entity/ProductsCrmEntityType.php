<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Products crm entity type entity.
 *
 * @ConfigEntityType(
 *   id = "products_crm_entity_type",
 *   label = @Translation("Products crm entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crm\ProductsCrmEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\crm\Form\ProductsCrmEntityTypeForm",
 *       "edit" = "Drupal\crm\Form\ProductsCrmEntityTypeForm",
 *       "delete" = "Drupal\crm\Form\ProductsCrmEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\crm\ProductsCrmEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "products_crm_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "products_crm_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/products_crm_entity_type/{products_crm_entity_type}",
 *     "add-form" = "/admin/structure/products_crm_entity_type/add",
 *     "edit-form" = "/admin/structure/products_crm_entity_type/{products_crm_entity_type}/edit",
 *     "delete-form" = "/admin/structure/products_crm_entity_type/{products_crm_entity_type}/delete",
 *     "collection" = "/admin/structure/products_crm_entity_type"
 *   }
 * )
 */
class ProductsCrmEntityType extends ConfigEntityBundleBase implements ProductsCrmEntityTypeInterface {

  /**
   * The Products crm entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Products crm entity type label.
   *
   * @var string
   */
  protected $label;

}
