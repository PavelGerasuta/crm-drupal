<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Orders crm entity entities.
 *
 * @ingroup crm
 */
interface OrdersCrmEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Orders crm entity name.
   *
   * @return string
   *   Name of the Orders crm entity.
   */
  public function getName();

  /**
   * Sets the Orders crm entity name.
   *
   * @param string $name
   *   The Orders crm entity name.
   *
   * @return \Drupal\crm\Entity\OrdersCrmEntityInterface
   *   The called Orders crm entity entity.
   */
  public function setName($name);

  /**
   * Gets the Orders crm entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Orders crm entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Orders crm entity creation timestamp.
   *
   * @param int $timestamp
   *   The Orders crm entity creation timestamp.
   *
   * @return \Drupal\crm\Entity\OrdersCrmEntityInterface
   *   The called Orders crm entity entity.
   */
  public function setCreatedTime($timestamp);

}
