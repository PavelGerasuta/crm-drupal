<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Orders crm entity type entities.
 */
interface OrdersCrmEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
