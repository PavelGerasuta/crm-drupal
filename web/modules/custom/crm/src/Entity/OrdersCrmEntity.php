<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Orders crm entity entity.
 *
 * @ingroup crm
 *
 * @ContentEntityType(
 *   id = "orders_crm_entity",
 *   label = @Translation("Orders crm entity"),
 *   bundle_label = @Translation("Orders crm entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crm\OrdersCrmEntityListBuilder",
 *     "views_data" = "Drupal\crm\Entity\OrdersCrmEntityViewsData",
 *     "translation" = "Drupal\crm\OrdersCrmEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\crm\Form\OrdersCrmEntityForm",
 *       "add" = "Drupal\crm\Form\OrdersCrmEntityForm",
 *       "edit" = "Drupal\crm\Form\OrdersCrmEntityForm",
 *       "delete" = "Drupal\crm\Form\OrdersCrmEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\crm\OrdersCrmEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\crm\OrdersCrmEntityAccessControlHandler",
 *   },
 *   base_table = "orders_crm_entity",
 *   data_table = "orders_crm_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer orders crm entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name_order",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/orders_crm_entity/{orders_crm_entity}",
 *     "add-page" = "/admin/structure/orders_crm_entity/add",
 *     "add-form" = "/admin/structure/orders_crm_entity/add/{orders_crm_entity_type}",
 *     "edit-form" = "/admin/structure/orders_crm_entity/{orders_crm_entity}/edit",
 *     "delete-form" = "/admin/structure/orders_crm_entity/{orders_crm_entity}/delete",
 *     "collection" = "/admin/structure/orders_crm_entity",
 *   },
 *   bundle_entity_type = "orders_crm_entity_type",
 *   field_ui_base_route = "entity.orders_crm_entity_type.edit_form"
 * )
 */
class OrdersCrmEntity extends ContentEntityBase implements OrdersCrmEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name_order')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name_order', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }


  public function getId(){
    return $this->get("id")->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }


  /**
   * @return mixed
   */
  public function getCourier() {
    return $this->get('courier')->target_id;
  }


  public function getCounterparties(){
    return $this->get('counterparties')->target_id;
  }


  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }



  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }


  /**
   * @return mixed
   */
  public function getOwnerName() {
    $owner_name = $this->get('user_id')->entity;
    return $owner_name->label();
  }


  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }



  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);



    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Orders entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Orders entity.'))
      ->setReadOnly(TRUE);


    $fields['name_order'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name order'))
      ->setDescription(t('The name of the Orders crm entity entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // counterparties заказа
    $fields['counterparties'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Counterparties'))
      ->setDescription(t('The clients to the order'))
      ->setSetting('target_type', 'counterparties_entity')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => -9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    // courier заказа
    $fields['courier'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Сourier'))
      ->setDescription(t('The courier to the order'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);




    $fields['closed'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t("Closed order"))
      ->setDisplayOptions('form', array(
        'type' => 'boolean_checkbox',
        'settings' => array(
          'display_label' => TRUE,
        ),
        'weight' => '10',
      ))
      ->setDisplayConfigurable('form', TRUE);


    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');



    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
