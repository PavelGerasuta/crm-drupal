<?php

namespace Drupal\crm\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Products crm entity entities.
 */
class ProductsCrmEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();


    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
