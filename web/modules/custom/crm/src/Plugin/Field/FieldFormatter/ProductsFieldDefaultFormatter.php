<?php

/**
 * @file
 * Contains \Drupal\crm\Plugin\Field\FieldFormatter\ProductsDefaultFormatter.
 */

namespace Drupal\crm\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;


/** *
 * @FieldFormatter(
 *   id = "products_field_default_formatter",
 *   label = @Translation("Default formatt"),
 *   field_types = {
 *     "products_field"
 *   }
 * )
 */
class ProductsFieldDefaultFormatter extends EntityReferenceLabelFormatter {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $values = $items->getValue();

    foreach ($elements as $delta => $entity) {
      $elements[$delta]['#suffix'] = ' количество = ' . $values[$delta]['count'];
    }

    return $elements;
  }
}
