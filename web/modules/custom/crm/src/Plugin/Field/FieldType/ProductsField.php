<?php

/**
 * @file
 * Contains Drupal\crm\Plugin\Field\FieldType\ProductsField.
 */

namespace Drupal\crm\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;



/**
 * @FieldType(
 *   id = "products_field",
 *   label = @Translation("Products field for order"),
 *   module = "crm",
 *   category = @Translation("CRM"),
 *   description = @Translation("Fields for adding products to an order."),
 *   default_widget = "products_field_widget",
 *   default_formatter = "products_field_default_formatter",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList"
 * )
 */

class ProductsField extends EntityReferenceItem {
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $quantity_definition = DataDefinition::create('integer')
      ->setLabel($field_definition->getSetting('count_label'));
    $properties['count'] = $quantity_definition;
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['count'] = array(
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
    );

    return $schema;
  }

}
