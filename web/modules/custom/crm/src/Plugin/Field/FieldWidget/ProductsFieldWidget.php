<?php

namespace Drupal\crm\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;



/**
 * @FieldWidget(
 *   id = "products_field_widget",
 *   module = "crm",
 *   label = @Translation("Order products field widget"),
 *   field_types = {
 *     "products_field"
 *   }
 * )
 */
class ProductsFieldWidget extends EntityReferenceAutocompleteWidget {

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = array(
      '#attributes' => ['class' => ['form--horizontal', 'clearfix']],
      '#theme_wrappers' => ['container'],
    );
    $widget['target_id'] = parent::formElement($items, $delta, $element, $form, $form_state);
    $widget['count'] = array(
      '#title' => $this->t('Count product'),
      '#type' => 'number',
      '#size' => '4',
      '#default_value' => isset($items[$delta]) ? $items[$delta]->count : 1,
      '#weight' => 10,
    );

    if ($this->fieldDefinition->getFieldStorageDefinition()->isMultiple()) {
      $widget['count']['#placeholder'] = $this->fieldDefinition->getSetting('count_label');
    }
    else {
      $widget['count']['#title'] = $this->fieldDefinition->getSetting('count_label');
    }

    return $widget;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    foreach ($values as $delta => $data) {
      if (empty($data['count'])) {
        unset($values[$delta]['count']);
      }
    }
    return $values;
  }

}
