<?php

namespace Drupal\crm;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Products crm entity entity.
 *
 * @see \Drupal\crm\Entity\ProductsCrmEntity.
 */
class ProductsCrmEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\products_for_crm\Entity\ProductsCrmEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished products crm entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published products crm entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit products crm entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete products crm entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add products crm entity entities');
  }


}
