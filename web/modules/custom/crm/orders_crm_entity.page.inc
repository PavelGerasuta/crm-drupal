<?php

/**
 * @file
 * Contains orders_crm_entity.page.inc.
 *
 * Page callback for Orders crm entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Orders crm entity templates.
 *
 * Default template: orders_crm_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_orders_crm_entity(array &$variables) {
  // Fetch OrdersCrmEntity Entity Object.
  $orders_crm_entity = $variables['elements']['#orders_crm_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
