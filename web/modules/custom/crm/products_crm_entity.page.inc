<?php

/**
 * @file
 * Contains products_crm_entity.page.inc.
 *
 * Page callback for Products crm entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Products crm entity templates.
 *
 * Default template: products_crm_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_products_crm_entity(array &$variables) {
  // Fetch ProductsCrmEntity Entity Object.
  $products_crm_entity = $variables['elements']['#products_crm_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
