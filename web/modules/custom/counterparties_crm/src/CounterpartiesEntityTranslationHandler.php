<?php

namespace Drupal\counterparties_crm;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for counterparties_entity.
 */
class CounterpartiesEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
