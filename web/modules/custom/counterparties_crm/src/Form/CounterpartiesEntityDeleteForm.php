<?php

namespace Drupal\counterparties_crm\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Counterparties entity entities.
 *
 * @ingroup counterparties_crm
 */
class CounterpartiesEntityDeleteForm extends ContentEntityDeleteForm {


}
