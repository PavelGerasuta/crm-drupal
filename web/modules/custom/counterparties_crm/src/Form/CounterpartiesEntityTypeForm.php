<?php

namespace Drupal\counterparties_crm\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CounterpartiesEntityTypeForm.
 */
class CounterpartiesEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $counterparties_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $counterparties_entity_type->label(),
      '#description' => $this->t("Label for the Counterparties entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $counterparties_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\counterparties_crm\Entity\CounterpartiesEntityType::load',
      ],
      '#disabled' => !$counterparties_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $counterparties_entity_type = $this->entity;
    $status = $counterparties_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Counterparties entity type.', [
          '%label' => $counterparties_entity_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Counterparties entity type.', [
          '%label' => $counterparties_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($counterparties_entity_type->toUrl('collection'));
  }

}
