<?php

namespace Drupal\counterparties_crm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Counterparties entity entities.
 *
 * @ingroup counterparties_crm
 */
class CounterpartiesEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Counterparties entity ID');
    $header['fullname'] = $this->t('Name counterparties');
    $header['bundle'] = $this->t('Type counterparties');
    $header['organization'] = $this->t('Organization');
    $header['user_id'] = $this->t('Own counterparties');
    $header['contact_person'] = $this->t('Contact person');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\counterparties_crm\Entity\CounterpartiesEntity $entity */
    $row['id'] = $entity->id();
    $row['fullname'] = Link::createFromRoute(
      $entity->label(),
      'entity.counterparties_entity.edit_form',
      ['counterparties_entity' => $entity->id()]
    );
    $row['bundle'] = $entity->bundle();
    $row['organization'] = $entity->organization->value;
    $row['user_id'] = $entity->getOwnerName();
    $row['contact_person'] = $entity->getContactPerson();
    return $row + parent::buildRow($entity);
  }

}
