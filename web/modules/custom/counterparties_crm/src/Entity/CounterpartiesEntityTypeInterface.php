<?php

namespace Drupal\counterparties_crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Counterparties entity type entities.
 */
interface CounterpartiesEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
