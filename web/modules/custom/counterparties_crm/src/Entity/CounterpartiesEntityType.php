<?php

namespace Drupal\counterparties_crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Counterparties entity type entity.
 *
 * @ConfigEntityType(
 *   id = "counterparties_entity_type",
 *   label = @Translation("Counterparties entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\counterparties_crm\CounterpartiesEntityTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\counterparties_crm\Form\CounterpartiesEntityTypeForm",
 *       "add" = "Drupal\counterparties_crm\Form\CounterpartiesEntityTypeForm",
 *       "edit" = "Drupal\counterparties_crm\Form\CounterpartiesEntityTypeForm",
 *       "delete" = "Drupal\counterparties_crm\Form\CounterpartiesEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\counterparties_crm\CounterpartiesEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "counterparties_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "counterparties_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/counterparties_entity_type/{counterparties_entity_type}",
 *     "add-form" = "/admin/structure/counterparties_entity_type/add",
 *     "edit-form" = "/admin/structure/counterparties_entity_type/{counterparties_entity_type}/edit",
 *     "delete-form" = "/admin/structure/counterparties_entity_type/{counterparties_entity_type}/delete",
 *     "collection" = "/admin/structure/counterparties_entity_type"
 *   }
 * )
 */
class CounterpartiesEntityType extends ConfigEntityBundleBase implements CounterpartiesEntityTypeInterface {

  /**
   * The Counterparties entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Counterparties entity type label.
   *
   * @var string
   */
  protected $label;

}
