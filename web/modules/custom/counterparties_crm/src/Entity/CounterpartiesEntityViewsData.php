<?php

namespace Drupal\counterparties_crm\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Counterparties entity entities.
 */
class CounterpartiesEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
