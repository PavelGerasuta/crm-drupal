<?php

namespace Drupal\counterparties_crm\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Counterparties entity entities.
 *
 * @ingroup counterparties_crm
 */
interface CounterpartiesEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Counterparties entity name.
   *
   * @return string
   *   Name of the Counterparties entity.
   */
  public function getName();

  /**
   * Sets the Counterparties entity name.
   *
   * @param string $name
   *   The Counterparties entity name.
   *
   * @return \Drupal\counterparties_crm\Entity\CounterpartiesEntityInterface
   *   The called Counterparties entity entity.
   */
  public function setName($name);

  /**
   * Gets the Counterparties entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Counterparties entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Counterparties entity creation timestamp.
   *
   * @param int $timestamp
   *   The Counterparties entity creation timestamp.
   *
   * @return \Drupal\counterparties_crm\Entity\CounterpartiesEntityInterface
   *   The called Counterparties entity entity.
   */
  public function setCreatedTime($timestamp);

}
