<?php

/**
 * @file
 * Contains counterparties_entity.page.inc.
 *
 * Page callback for Counterparties entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Counterparties entity templates.
 *
 * Default template: counterparties_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_counterparties_entity(array &$variables) {
  // Fetch CounterpartiesEntity Entity Object.
  $counterparties_entity = $variables['elements']['#counterparties_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
