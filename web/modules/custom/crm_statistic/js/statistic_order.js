(function () {
  'use strict';

  // Recovering the orders_users from drupalSettings.
  let orders_users  = drupalSettings.data.orders_users;

  console.log(orders_users);
  let i =0;
  let labels = [],data=[];
  for (const [key, value] of Object.entries(orders_users)) {
    //console.log(`${key}: ${value}`);
    labels[i] = key;
    data[i] = value;
    i++;
  }


  let ctx = document.getElementById('OrdersChart').getContext('2d');
  let myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
        label: 'Заказы',
        data: data,
        backgroundColor: [
          'rgba(216, 27, 96, 0.6)',
          'rgba(3, 169, 244, 0.6)',
          'rgba(255, 152, 0, 0.6)',
          'rgba(29, 233, 182, 0.6)',
          'rgba(156, 39, 176, 0.6)',
          'rgba(84, 110, 122, 0.6)'
        ],
        borderColor: [
          'rgba(216, 27, 96, 1)',
          'rgba(3, 169, 244, 1)',
          'rgba(255, 152, 0, 1)',
          'rgba(29, 233, 182, 1)',
          'rgba(156, 39, 176, 1)',
          'rgba(84, 110, 122, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      legend: {
        display: false
      },
      title: {
        display: true,
        text: 'The number of orders',
        position: 'top',
        fontSize: 16,
        padding: 20
      },
      scales: {
        yAxes: [{
          ticks: {
            stepSize: 1,
            beginAtZero: true,
          }
        }]
      }
    }
  });



})();
