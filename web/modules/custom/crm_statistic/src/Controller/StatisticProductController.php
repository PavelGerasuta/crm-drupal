<?php

namespace Drupal\crm_statistic\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class StatisticProductController
 *
 * @package Drupal\crm_statistic\Controller
 */

class StatisticProductController extends ControllerBase{


  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function content(){

    $build['content'] = [
      '#theme' => 'statistic_product',
    ];

    $orders = \Drupal::entityTypeManager()->getStorage('orders_crm_entity')->loadMultiple();


    $products_users = \Drupal::service('crm.building_product_services')->products_users_build($orders);


    $build['#attached']['library'][] = 'crm_statistic/chart_js';
    $build['#attached']['library'][] = 'crm_statistic/statistic_product_js';
    $build['content']['#attached']['drupalSettings']['data']['products_users'] = $products_users;




    return $build;
  }





}
