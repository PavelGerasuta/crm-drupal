<?php

namespace Drupal\crm_statistic\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines StatisticOrderController class.
 */
class StatisticOrderController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {

    $orders = \Drupal::entityTypeManager()->getStorage('orders_crm_entity')->loadMultiple();

    $orders_users = \Drupal::service('crm.building_product_services')->orders_users_build($orders);


    $build['content'] = [
      '#theme' => 'statistic_order',
    ];

    $build['#attached']['library'][] = 'crm_statistic/chart_js';
    $build['#attached']['library'][] = 'crm_statistic/statistic_order_js';
    $build['content']['#attached']['drupalSettings']['data']['orders_users'] = $orders_users;

    return $build;

  }




}
